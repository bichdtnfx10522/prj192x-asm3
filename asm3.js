var data = [{
    name: "Phạm Văn A",
    math: 5,
    physical: 6,
    chemistry: 7
},
{
    name: "Trần Thị B",
    math: 8,
    physical: 9,
    chemistry: 9
}
];

var testNumber = /^[0-9.-]+$/
var showAverage = false;
var showGoodStudent = false;

function makeCell(text, rownum, colnum) {
    var cell = $("<td></td>").text(text);
    cell.attr("id", "colScoreTable-" + rownum + "-" + colnum);
    return cell;
}
function makeRow(rownum) {

    var row = $("<tr></tr>");
    row.attr("id", "rowScoreTable" + rownum);

    var cell = $("<td></td>").html('<input type="checkbox" name="student" id = "checkboxScore-' + rownum + '" />');
    cell.attr("id", "colScoreTable-" + rownum + "-0");

    var cell1 = makeCell(rownum, rownum, 1);
    var cell2 = makeCell(data[rownum - 1].name, rownum, 2);
    var cell3 = makeCell(data[rownum - 1].math, rownum, 3);
    var cell4 = makeCell(data[rownum - 1].physical, rownum, 4);
    var cell5 = makeCell(data[rownum - 1].chemistry, rownum, 5);
    var cell6 = "";

    if (!showAverage) {
        cell6 = makeCell("?", rownum, 6);
    } else {
        cell6 = makeCell(average(rownum), rownum, 6);
    }

    row.append(cell, cell1, cell2, cell3, cell4, cell5, cell6);

    showAverage = false;
    showGoodStudent = false;
    return row;
}

function average(num) {
    var sum = (parseFloat(data[num - 1].math) + parseFloat(data[num - 1].physical) + parseFloat(data[num - 1].chemistry)) / 3;
    var average = sum.toFixed(1);
    return average;
}
function loadData() {
    for (let i = 1; i <= data.length; i++) {
        $("#result").append(makeRow(i));
    }
}
$(document).ready(function () {

    loadData();

    $("form").submit(function () {

        showAverage = false;
        showGoodStudent = false;

        var testScore = {
            name: "",
            math: 0,
            physical: 0,
            chemistry: 0
        };
        testScore.name = $("#name").val();
        testScore.math = $("#math").val();
        testScore.physical = $("#physical").val();
        testScore.chemistry = $("#chemistry").val();

        if (!testNumber.test(testScore.math) || !testNumber.test(testScore.physical) || !testNumber.test(testScore.chemistry)) {

            alert("Nhập điểm không hợp lệ");
            return false;

        }
        if (testScore.math > 10 || testScore.math < 0 || testScore.physical > 10 || testScore.physical < 0 || testScore.chemistry > 10
            || testScore.chemistry < 0) {

            alert("Điểm Toán Lý Hóa lớn hơn hoặc bằng 0 và nhỏ hơn hoặc bằng 10");
            return false;

        }
        $("#name").val("");
        $("#math").val("");
        $("#physical").val("");
        $("#chemistry").val("");

        data.push(testScore);
        $("#result").append(makeRow(data.length));

    });

    $("#average-score").click(function () {

        showAverage = true;
        for (i = 1; i <= data.length; i++) {
            var idCell = "colScoreTable-" + i + "-6";
            $("#" + idCell).text(average(i));
        }
    });

    $("#show-good-student").click(function () {

        showGoodStudent = true;

        $("tr td:last-child").each(function () {
            if ($(this).text() == "?") {
                alert("Bạn chưa tính hết điểm trung bình của tất cả học sinh.");
                return false;
            }
            if ($(this).text() >= 8) {
                $(this).parent().css("color", "red");
            }
        })
    });
    $("#check-all").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    $("#repair-student").click(function () {


        for (var i = 1; i <= data.length; i++) {

            if ($("#checkboxScore-" + i).is(":checked")) {
                for (let j = 2; j < 6; j++) {
                    var val = $("#colScoreTable-" + i + "-" + j).text();
                    $("#colScoreTable-" + i + "-" + j).html('<input type = "text" id = "textboxStudent-'
                        + i + '-' + j + '" value = "' + val + '"></input>');

                }
            }

        }
        $("#repair-student").hide();
        $("#repair-student-success").show();
    });

    $("#repair-student-success").click(function () {
        for (let i = 1; i <= data.length; i++) {
            if ($("#checkboxScore-" + i).is(":checked")) {

                var testScore = {
                    name: "",
                    math: 0,
                    physical: 0,
                    chemistry: 0
                };
                for (let j = 2; j < 6; j++) {
                    var value = $("#textboxStudent-" + i + "-" + j).val();
                    if (j != 2) {
                        if (!testNumber.test(value)) {

                            alert("Điểm ở dòng " + i + " phải là số");
                            break;
                        }

                        if (value > 10 || value < 0) {
                            alert("Điểm ở dòng " + i + " phải lớn hơn hoặc bằng 0 và nhỏ hơn hoặc bằng 10");
                            break;
                        }
                    }
                    switch (j) {
                        case 2:
                            testScore.name = value;
                            break;
                        case 3:
                            testScore.math = value;
                            break;
                        case 4:
                            testScore.physical = value;
                            break;
                        case 5:
                            testScore.chemistry = value;
                            break;
                        
                    }
                    data[i - 1] = testScore;
                    $("#colScoreTable-" + i + "-" + j).text(value);
                    $("#colScoreTable-" + i + "-6").text("?");
                    showAverage = false;
                    showGoodStudent = false;

                }
            }

        }
        $("#repair-student").show();
        $("#repair-student-success").hide();
    });

    $("#delete-student").click(function () {

        var check = false;
        var deleteArray = [];

        for (let i = 1; i <= data.length; i++) {
            if ($("#checkboxScore-" + i).is(":checked")) {
                check = true;
                deleteArray.push(i-1);
            }
        }
        for (var i = 0; i < deleteArray.length; i++) {
            data.splice(deleteArray[i], 1);  
        }    
        $("#result").html(" <tr><th><input type='checkbox' name='student'  id='check-all'/>"
        + "</th><th>STT</th><th>Họ tên</th><th>Toán</th><th>Lý</th><th>Hóa</th><th>Điểm trung bình</th></tr>");
        loadData();
        if(!check){
            alert("Chọn học sinh mà bạn muốn xóa");
        }
    });
});

